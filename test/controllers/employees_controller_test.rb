require 'test_helper'

class EmployeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @employee = employees(:one)
  end

  test "should get index" do
    get employees_url
    assert_response :success
  end

  test "should get new" do
    get new_employee_url
    assert_response :success
  end

  test "should create employee" do
    assert_difference('Employee.count') do
      post employees_url, params: { employee: { date_of_birth: @employee.date_of_birth, department: @employee.department, employee_name: @employee.employee_name, employee_no: @employee.employee_no, employee_type: @employee.employee_type, employement_status: @employee.employement_status, gender: @employee.gender, original_assignment: @employee.original_assignment, permanent_address: @employee.permanent_address, primary_contact_no: @employee.primary_contact_no, primary_email_address: @employee.primary_email_address, rank: @employee.rank, secondary_contact_no: @employee.secondary_contact_no, secondary_email_address: @employee.secondary_email_address, unit: @employee.unit } }
    end

    assert_redirected_to employee_url(Employee.last)
  end

  test "should show employee" do
    get employee_url(@employee)
    assert_response :success
  end

  test "should get edit" do
    get edit_employee_url(@employee)
    assert_response :success
  end

  test "should update employee" do
    patch employee_url(@employee), params: { employee: { date_of_birth: @employee.date_of_birth, department: @employee.department, employee_name: @employee.employee_name, employee_no: @employee.employee_no, employee_type: @employee.employee_type, employement_status: @employee.employement_status, gender: @employee.gender, original_assignment: @employee.original_assignment, permanent_address: @employee.permanent_address, primary_contact_no: @employee.primary_contact_no, primary_email_address: @employee.primary_email_address, rank: @employee.rank, secondary_contact_no: @employee.secondary_contact_no, secondary_email_address: @employee.secondary_email_address, unit: @employee.unit } }
    assert_redirected_to employee_url(@employee)
  end

  test "should destroy employee" do
    assert_difference('Employee.count', -1) do
      delete employee_url(@employee)
    end

    assert_redirected_to employees_url
  end
end
