json.extract! transaction, :id, :TransactionNo, :ApplicationDate, :LeaveType, :LeaveStatus, :DevelopmentType, :DegreePursued, :Institution, :Location, :Country, :SponsorDonor, :LocalAbroad, :StartDate, :EndDate, :Duration, :ReportForDuty, :ReturnServiceObligation, :ReturnServiceObligationStatus, :created_at, :updated_at
json.url transaction_url(transaction, format: :json)
