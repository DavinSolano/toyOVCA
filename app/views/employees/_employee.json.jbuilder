json.extract! employee, :id, :employee_no, :employee_name, :date_of_birth, :gender, :primary_email_address, :secondary_email_address, :primary_contact_no, :secondary_contact_no, :permanent_address, :unit, :department, :employee_type, :employement_status, :rank, :original_assignment, :created_at, :updated_at
json.url employee_url(employee, format: :json)
