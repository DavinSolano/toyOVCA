Rails.application.routes.draw do
  resources :employees
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations'}
  resources :transactions do
  collection { post :import }
  end
  resources :employees do
  collection { post :import }
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'static_pages#home'



  get  '/transactions' => 'transactions#index'
  get  '/employees' =>  'employees#index'

end
