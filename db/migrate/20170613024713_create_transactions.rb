class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.string :TransactionNo
      t.string :ApplicationDate
      t.string :LeaveType
      t.string :LeaveDetails
      t.string :LeaveStatus
      t.string :DevelopmentType
      t.string :DegreePursued
      t.string :Institution
      t.string :Location
      t.string :Country
      t.string :SponsorDonor
      t.string :LocalAbroad
      t.string :StartDate
      t.string :EndDate
      t.string :Duration
      t.string :ReportForDuty
      t.string :ReturnServiceObligation
      t.string :ReturnServiceObligationStatus

      t.timestamps
    end
  end
end
